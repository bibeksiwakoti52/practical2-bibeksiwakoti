import junit.framework.TestCase;

public class Geometry3DTest extends TestCase{

    public void testVolumeCuboid() {
        Geometry3D cuboid = new Geometry3D();
        int length = 5;
        int width = 8;
        int height = 2;

        int actual = cuboid.volumeCuboid(length,width,height);

        int expected = 80;

        assertEquals(expected,actual);
    }
    
    public void testSurfaceAreaCuboid() {
        Geometry3D cuboid = new Geometry3D();
        int length = 5;
        int width = 8;
        int height = 2;

        int actual = cuboid.surfaceAreaCuboid(length, width, height);

        int expected = 132;

        assertEquals(expected,actual);
    }
    
    public void testVolumePyramid() {
        Geometry3D pyramid = new Geometry3D();
        int base = 3;
        int height = 7;

        float actual = pyramid.volumePyramid(base, height);

        float expected = 21;

        assertEquals(expected,actual, 0.1);
    }
    
    public void testSurfaceAreaPyramid() {
        Geometry3D pyramid = new Geometry3D();
        int base = 3;
        int height = 7;

        float actual = pyramid.surfaceAreaPyramid(base, height);

        float expected =51.953f;

        assertEquals(expected,actual, 0.1);
    }
    
    public void testVolumeTetrahedron() {
        Geometry3D tetrahedron = new Geometry3D();
        int a = 5;

        float actual = tetrahedron.volumeTetrahedron(a);

        float expected = 14.73f;

        assertEquals(expected,actual, 0.1);
    }
    
    public void testSurfaceAreaTetrahedron() {
        Geometry3D tetrahedron = new Geometry3D();
        int a = 5;

        float actual = tetrahedron.surfaceAreaTetrahedron(a);

        float expected = 43.30f;

        assertEquals(expected,actual, 0.1);
    }
}