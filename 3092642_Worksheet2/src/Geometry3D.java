 
public class Geometry3D {
	
	int volumeCuboid(int length, int width, int height) {
        int volume = length*width*height;
        return volume;
    }
	
    int surfaceAreaCuboid(int length,int width,int height) {
        int surfaceArea = 2 * ((length * width)+(length*height)+(width*height));
        return surfaceArea;
    }
    
    float volumePyramid(int b, int h) {
        float volume =  ((b * b) * h) /3;
        return volume;
    }
    
    float surfaceAreaPyramid(int b, int h) {
        float surfaceArea = (float) ((b*b)+(b*Math.sqrt((b*b)+(4*h*h))));
        return surfaceArea;
    }
    
    float volumeTetrahedron(int a) {
        float volume = (float) ((a*a*a)/(6 *Math.sqrt(2)));
        return volume;
    }
    
    float surfaceAreaTetrahedron(int a) {
        float volume = (float) (Math.sqrt(3)*a*a);
        return volume;
    }

}
